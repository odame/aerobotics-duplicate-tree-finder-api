# Solutions
### Naive Approach

The remote API can return a maximum of 10000 objects in a single request
So data is fetched in chunks

data_chunks = [[...10000 TreeSurvey objects], [...10000 TreeSurvey objects], ...]

number_of_duplicate_trees = 0

forEach chunk in data_chunks {
    forEach treeSurveyObject in chunk {

        Convert the long/lat of the object into a form that allows us to 
        compare both components together
        
        position = '${long_to_7dp}${lat_to_7dp}'

        Convert the object's tree_survey_id and tree_id into a form that allows
            us to compare both together
        
        treeSurveyKey = '${tree_survey_id}${tree_id}'

        if there are other objects with the same position
        but, they are of the different treeSurveyKeys, then it constitutes a duplicate

        number_of_duplicate_trees += 1
    }
}




# Challenges
* Data is in chunks of 10000 TreeSurvey objects.
This means multiple network calls will have to be made and this slows down the API.

Possible workaround will be to load data chunks in parallel (memory constraints has to be taken into account though)
