from unittest import TestCase
from .mocks import fake_api_service as api_service
from src.data_service import DataService
from src.duplicate_finder import DupFinder


class DuplicateFinderTestCase(TestCase):
    orchard_id = 518

    def setUp(self):
        data_service = DataService(api_service)
        self.dup_finder = DupFinder(data_service)

    def testFindDuplicatesForOrchard(self):
        result = self.dup_finder.get_num_duplicate_trees_for_orchard(
            self.orchard_id
        )

        self.assertEqual(
            result,
            1017,
            'Number of duplicate trees is must be equal to 1017'
        )
