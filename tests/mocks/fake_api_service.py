import json

BASE_URL = ''
TREE_SURVEYS_PATH = ''


def get(**kwargs) -> dict:
    response = {}
    with open('./tests/mocks/response_with_duplicates.json', 'r') as json_file:
        response = json.load(json_file)
    return response


def post():
    raise RuntimeError('Not implemented yet')


def put():
    raise RuntimeError('Not implemented yet')
