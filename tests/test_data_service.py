from .mocks import fake_api_service as APIService
from src.data_service import DataService
from unittest import TestCase
from types import GeneratorType


class GetTreeSurveysTestCase(TestCase):
    orchard_id = 518

    def setUp(self):
        self.data_service = DataService(APIService)

    def testGetTreesSurveysData(self):
        tree_surveys = self.data_service.get_tree_surveys_chunks_for_orchard(
            self.orchard_id
        )

        self.assertIsInstance(
            tree_surveys, GeneratorType,
            'Returned data must be a generator, yielded in chunks'
        )
        self.assertIsNotNone(
            next(tree_surveys),
            'Returned data must yield on next()'
        )
