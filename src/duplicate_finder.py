from collections import defaultdict
from .data_service import DataService


class DupFinder():
    def __init__(self, data_service: DataService):
        self.data_service = data_service

    def __get_tree_position_key(self, tree_survey: dict):
        """Get a key that we will use to uniquely identify a tree.

        Key is of the form `${long_rounded_to_7dp}${lat_rounded_to_7dp}`
        """

        return str(round(tree_survey['longitude'], 7)) + \
            str(round(tree_survey['latitude'], 7))

        # NB: We can also use hash().
        #   The hash for two immutable of the same sequence of elements will be
        #   the same
        # hash((round(item['longitude'], 7), round(item['latitude'], 7)))

    def __get_tree_survey_key(self, tree_survey: dict):
        """ Get a key that can be used to determine if two objects have the same 
        tree_survey_id and the same tree_id.

        Key is of the form `${tree_survey_id}${tree_id}`
        """
        return str(tree_survey['survey_id']) + str(tree_survey['tree_id'])

    def get_num_duplicate_trees_for_orchard(self, orchard_id: int) -> int:
        """ Given an orchard's id, return the number of trees that are
        duplicates of others
        """
        # the key for each entry is of the same format as __get_key()
        # values will be a list of the duplicated trees
        trees_at_position = defaultdict(dict)
        num_duplicates = 0

        data_chunks = self.data_service.get_tree_surveys_chunks_for_orchard(
            orchard_id)
        # this for-loop, though nested, has a time complexity close to O(n)
        for chunk in data_chunks:
            for tree_survey in chunk:
                # For two trees to be even considered for duplication,
                # they must have similar coordinates

                # And they must have different survey ids and tree ids

                # a combination of both long and lat rounded to 7 dp
                # Allows for conveniently comparing both long and lat
                # within 7 decimal places
                position_key = self.__get_tree_position_key(tree_survey)

                # a combination of the tree's survey_id and tree_id
                # allows to conveniently compare BOTH survey_id and tree_id for
                # equality
                survey_key = self.__get_tree_survey_key(tree_survey)

                other_trees_in_same_position = trees_at_position[position_key]

                # if there are other trees in the same position,
                # but they dont have the same survey_id and tree_id as this
                # one, then we have a duplicate
                if (
                    other_trees_in_same_position and
                        survey_key not in other_trees_in_same_position
                ):
                    num_duplicates += 1

                # save this tree as having been found in the this position
                other_trees_in_same_position[survey_key] = True

        del trees_at_position

        return num_duplicates
