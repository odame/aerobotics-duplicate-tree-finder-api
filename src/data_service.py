""" Functionality for accessing data needed by the application"""
from .api_service import APIService, TREE_SURVEYS_PATH, DATA_LIMIT


class DataService(object):
    """ Wraps functionality for accessing data needed by the application """

    def __init__(self, api_service: APIService):
        self.api_service = api_service

    def get_tree_surveys_chunks_for_orchard(
        self, orchard_id: int
    ):
        """Generator to get the tree surveys of an orchard in chunks

        Arguments:
            orchard_id {int} -- Id of the orchard for which to get the tree
            surveys

        Returns:
            Yields tree survey objects in chunks (as a list) of :chunk_size:
        """

        json_response = self.api_service.get(
            path=TREE_SURVEYS_PATH,
            query_params={
                'survey__orchard_id': orchard_id, 'limit': DATA_LIMIT
            },
            prefix_base_url=True
        )

        next_chunk_url = json_response['next']

        if next_chunk_url:  # there is more data to be fetched
            yield json_response['results']

            # continue yielding until there is no more data from the server
            while True:
                # next_chunk_url already has 'limit', 'offset'
                # and 'survey__orchard_id' params
                json_response = self.api_service.get(
                    path=next_chunk_url, base_url='', prefix_base_url=False
                )

                next_chunk_url = json_response['next']
                if next_chunk_url:
                    yield json_response['results']
                else:
                    break
        else:
            yield json_response['results']
