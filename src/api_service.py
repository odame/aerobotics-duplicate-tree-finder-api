""" Super lightweight collection of functions to make HTTP calls to API """

import requests
import json
from . import config


BASE_URL = config.AEROBOTICS_API_BASE_URL
TREE_SURVEYS_PATH = '/treesurveys/'

DATA_LIMIT = 10000  # the api can return a max of 10000 items in a request

DEFAULT_HEADERS = {
    'Authorization': config.AEROBOTICS_API_TOKEN
}


class APIService():
    """ Super lightweight collection of functions to make HTTP calls to API """

    def get(
        self, path: str, query_params: dict = None, prefix_base_url=True
    ) -> dict:
        """Make a GET request for a resource

        Arguments:
            path {str} -- The path of the resource.

        Keyword Arguments:
            query_params {dict} -- Key-value pairs to be passed as
                query params.

        Returns:
            dict -- The json data contained in the response from the request
                If there is no JSON data found in the response,
                the raw response data is returned.

        Raises:
            requests.HTTPError -- If the request failed
        """

        url = (BASE_URL + path) if prefix_base_url else path
        response = requests.get(url, params=query_params,
                                headers=DEFAULT_HEADERS)
        response.raise_for_status()
        json_string = response.text
        response_data = json.loads(json_string)

        return response_data

    def post(self):
        raise RuntimeError('Not implemented yet')

    def put(self):
        raise RuntimeError('Not implemented yet')
