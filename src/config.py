""" Application configuration/settings """

import os
AEROBOTICS_API_TOKEN = os.environ.get('AEROBOTICS_API_TOKEN', '')
AEROBOTICS_API_BASE_URL = 'https://sherlock.aerobotics.io/developers'
