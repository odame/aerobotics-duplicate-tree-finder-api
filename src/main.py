from flask import Flask, make_response, jsonify
# fixme: too many imports
# A Dependency Injection framework can save us a lot of lines :)
from .duplicate_finder import DupFinder
from .data_service import DataService
from .api_service import APIService

APP = Flask(__name__)


@APP.route('/aerobotics/find-duplicate-trees-for-orchard/<int:orchard_id>/')
def find_num_duplicates(orchard_id: int):
    dup_finder = DupFinder(DataService(APIService()))

    num_duplicate_trees = \
        dup_finder.get_num_duplicate_trees_for_orchard(
            orchard_id
        )

    return make_response(
        # convert to a json format for transport over http
        jsonify({'num_duplicate_trees': num_duplicate_trees}), 200
    )


@APP.route('/')
@APP.route('/hello-world/')
def hello():
    return '''
        Hello World.
        This is a simple API for finding duplicate trees in an orchard
    ''', 200
