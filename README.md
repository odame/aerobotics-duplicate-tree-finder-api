# [Aerobotics][5] Duplicate Tree Finder

Super lightweight API to get duplicated trees in an orchard.

#
## Overview
Each orchard in the database is a separate entry. 
When a client flies the drone it creates a survey object which has an orchard_id
When ML algorithms run, tree objects are created, each containing an orchard_id.
This in turn creates treeSurvey objects which contain a tree_id and and a survey_id

So as an example. If we have an orchard containing 1000 trees that has been flown 5 times.
There should be the following in our database:
- 1 orchard object
- 5 survey objects
- 1000 tree objects
- 5000 tree survey objects

We unfortunately have cases where we might have 5000 tree objects instead of 1000. And so if there are 2 treeSurveys which have a similar lat lng (to 7 decimal places), different survey_ids and different tree_ids then it is regarded as a duplicate tree.


## Usage
Base url for this API is ``.
For detailed usage information, consult the [Postman Docs][1]


## Dependencies
This API written in `Python3.7` and hosted on [Heroku][4].
See `requirements.txt` for packages this project uses


## Testing
This project uses python's in-built [`unitttest`][2] package for testing.
You may however drive tests with [`pytest`][3] or any other `unittest` compatible testing tool




[1]: https://documenter.getpostman.com/view/443714/Rzfnhkh9
[2]: https://docs.python.org/3/library/unittest.html
[3]: https://nose.readthedocs.io/en/latest/index.html
[4]: https://docs.pytest.org
[5]: https://aerobotics.io
